import boto3
import json
import time
import os
import stat
import logging
import shutil
import angr
import claripy
import md5
import re

logger = logging.getLogger()
logger.setLevel(logging.WARNING)

def make_response(d, method, status=200):
    """
    (dict[any]:any) -> dict[any]:any
    This turns return data into a string the API
     can process.
    """
    res = { "isBase64Encoded": False,
            "statusCode": status,
            "headers": {
                "Access-Control-Allow-Origin": "*"},
            "body": json.dumps(d),
            "method": method
            }
    return res

MAXLEN = 30

def lambda_handler(event,context):
    LOCAL_BINARY = "/tmp/binary"
    shutil.copy("root/home/fas/cb-multios/build/challenges/CGC_Hangman_Game/CGC_Hangman_Game",
            LOCAL_BINARY)
    binary_name = LOCAL_BINARY
    st = os.stat(LOCAL_BINARY)
    os.chmod(LOCAL_BINARY, st.st_mode | stat.S_IEXEC)

    ld_path = "root/lib/i386-linux-gnu/"
    testcase= "HANGEMHIGH!\n"
    testcase= "A"*len(testcase)

    p = angr.Project(binary_name,
        except_missing_libs=True,
        custom_ld_path=ld_path)

    state = p.factory.full_init_state(add_options=angr.options.unicorn)
    state.posix.files[0].length=MAXLEN

    t = Tracer()
    t.run_to_end(testcase,state)

    print "done."
    for key in t.weight_table:
        pair, cases = t.weight_table[key]
        print "{}: [{},\n{}]".format(hex(key),hex(pair),json.dumps(list(cases),indent=2))
    t.highest_weight()
    return make_response("done","default")

class Tracer():
    def __init__(self):
        self.weight_table = dict()
        self.blacklist = set()

    def make_entry(self,addr1,addr2):
        if (addr1 not in self.weight_table and
            addr2 not in self.weight_table):
            self.weight_table[addr1] = (addr2,set())
            self.weight_table[addr2] = (addr1,set())

    def add_testcase(self,testcase_name,addr):
        (_,tmp) = self.weight_table.get(addr,(0,set()))
        tmp.add(testcase_name)

    def highest_weight(self):
        testcase_values = dict()
        m = (0,"")
        for key in self.weight_table:
            (pair,testcases) = self.weight_table[key]
            (_,pairs_testcases) = self.weight_table[pair]
            if len(pairs_testcases)==0:
                for testcase in testcases:
                    v = testcase_values.get(testcase,0)+1
                    testcase_values[testcase]=v
                    if testcase not in self.blacklist and v>m[0]:
                        m = (v,testcase)
        print json.dumps(testcase_values,indent=2)
        with open("/tmp/tests/{}".format(m[1]), "rb") as f:
            data = f.read()
        print "{}:{}".format(m[1],data.encode('hex'))
        return data

    def run_to_end(self, testcase, state, flip=True, blacklist=True):
        testcase_name = md5.new(testcase).hexdigest()
        self.scase(testcase,testcase_name)
        if blacklist:
            self.blacklist.add(testcase_name)
        while True:
            succ = state.step()
            if len(succ.successors)==2:
                state1, state2 = succ.successors
                # ensure both states are in weight_table
                self.make_entry(state1.addr, state2.addr)                
                valid1= self.check_sat(state1, testcase, flip)
                valid2= self.check_sat(state2, testcase, flip)
                if valid1 ^ valid2:
                    state = state1 if valid1 else state2
                    #add testcase to state key in weight_table
                    self.add_testcase(testcase_name,state.addr)
                else:
                    break
            elif len(succ.successors):
                state = succ.successors[0]
            else:
                break

    def scase(self, s, fname):
        s = s[:MAXLEN]
        print "NEW TESTCASE:\n{}".format(s)
        print "ENCODED:\n{}".format(s.encode('hex'))
        if not os.path.exists('/tmp/tests'):
            os.makedirs('/tmp/tests')
        with open("/tmp/tests/{}".format(fname), "wb") as f:
            f.write(s)

    def check_sat(self, state,testcase,flip=True):
        stdin = state.posix.files[0]
        pos = stdin.pos
        stdin.seek(0)
        cond = map(lambda x: stdin.read_from(1)==x,
            testcase) if testcase else []
        stdin.seek(pos)
        try:
            state.solver.eval(stdin.all_bytes(), 
                extra_constraints=cond,
                cast_to=str)
            return True
        except angr.errors.SimUnsatError:
            pass
        if flip:
            try:
                self.run_to_end(state.solver.eval(stdin.all_bytes(),
                    cast_to=str), state, flip=False, blacklist=False)
            except angr.errors.SimUnsatError:
                pass        
        return False