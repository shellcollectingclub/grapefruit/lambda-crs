import logging

import lambdautils as lutil

FORMAT = "[%(filename)s:%(lineno)s - %(funcName)20s()] %(message)s"
logging.basicConfig(format=FORMAT)
logger = logging.getLogger()
logger.setLevel(logging.INFO)

bucket = "pcaps.shellcollecting.club"

def lambda_handler(event, context):
	logger.debug("Event: %s", event)
	method = event.get("httpMethod","")
	resource = event.get("resource", "")
	logger.info("Method: %s -- Resource: %s", method,resource)

	if method=="PUT" and resource=="/upload-binary":
		logger.info("Processing upload.")
		name = lutil.event_data_as_binary(event,
				"/tmp/",encoding=lambda x:x)
		location = "/tmp/{}".format(name)
		key = "services/{}".format(name)
		if lutil.upload(bucket, key, location):
			return lutil.response({"s3-key": key})
	elif method=="POST" and resource=="/start-fuzzer":
		logger.info("Starting fuzzer")
		parsed = lutil.event_data_as_dict(event,
			("fuzzer", unicode),
			("timeout", int),
			("service", unicode))
		if parsed:
			(fuzzer, timeout, servicekey) = parsed
			logger.info("data: %s", parsed)
			#TODO spawn lambda with /tmp/fuzzer-code.zip and 
			# angr.lambda_handler if fuzzer==angr etc
			return lutil.response("Success!")
	elif method=="POST" and resource=="/cmin":
		pass
	elif method=="POST" and resource=="/tmin":
		pass

	return lutil.response("Failure!", 400)