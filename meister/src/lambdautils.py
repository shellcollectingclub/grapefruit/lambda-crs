import boto3
import json
import os
import stat
from base64 import b64decode
import md5
import logging
from zipfile import ZipFile

# get log object
logger = logging.getLogger()

# create an S3 session
s3 = boto3.resource('s3')
s3_client = boto3.client('s3')

def event_data_as_dict(event,*keys):
    logging.debug("Looking for keys %s in event.", keys)
    try:
        data = json.loads(event["body"])
        if ((type(data) != dict) or 
            any(((key not in data) or 
                (type(data[key]) != ktype)) for key, ktype in keys)):
            raise TypeError("Incorrect body type: {}".format(data))
        logger.debug("POSTED: %s", data)
        return [data[key] for key,_ in keys]
    except Exception as e:
        logging.warning("Error loading request body: %s", e)
        return None

def make_executable(location):
    st = os.stat(location)
    os.chmod(location, st.st_mode | stat.S_IEXEC)

def event_data_as_binary(event, location, encoding=b64decode):
    logger.info("Saving binary to %s", location)
    if not in_tmp(location):
        return False
    try:
        logger.debug("Writing %s bytes of %s", 
                len(event['body']), 
                type(event['body']))
        data = encoding(event["body"])
        name = md5.new(data).hexdigest()
        with open(location+name, "wb") as f:
            f.write(data)
        return name
    except:
        logger.warning("Unable to save binary: %s", e)
        return False
    return True

def resolve_keys(bucket, prefix):
    return list(s3.Bucket(
        bucket).objects.filter(Prefix=prefix))

def in_tmp(location):
    prefix = "/tmp/"
    return (location[:len(prefix)] == prefix and
            "../" not in location)

def download(bucket, key, location):
    logger.info("Updating %s.", location)
    if not in_tmp(location):
        return False
    try:
        LOCAL_VERSION = "{}.version".format(location)
        logger.debug("Checking cache version...")
        data = ""
        # Make sure the local version file exists.
        if os.path.isfile(LOCAL_VERSION):
            with open(LOCAL_VERSION, "r") as f:
                data = f.read()
        # This get_object will error (304) if the
        #  ETag has not changed. We will use the
        #  local copy if this is the case.
        s3key = s3_client.get_object(Bucket=bucket,
                Key=key,
                IfNoneMatch=data)
        # There was no error, so download the new
        #  version of the filter.
        logger.info("Downloading %s to %s.", key, location)
        s3_client.download_file(Bucket=bucket,
            Key=key,
            Filename=location)
        # Save the version tag so we know when to
        #  update our local copy.
        with open(LOCAL_VERSION, "w") as f:
            f.write(s3key["ETag"])
    except Exception as e:
        logger.warning("Error updating %s: %s", location, e)
        return False
    return True

def upload(bucket, key, location):
    logger.info("Writing %s to s3://%s.", location, key)
    if not in_tmp(location):
        return False
    try:
        LOCAL_VERSION = "{}.version".format(location)
        s3_client.upload_file(Bucket=bucket,
                Key=key,
                Filename=location)
        s3key = s3_client.get_object(Bucket=bucket,
                Key=key)
        with open(LOCAL_VERSION, "w") as f:
            f.write(s3key["ETag"])
    except Exception as e:
        logger.warning("Failed to upload %s: %s", location, e)
        return False
    return True

def response(d, code=200, meta=None):
    res = { "isBase64Encoded": False,
        "statusCode": 200,
        "headers": { "Access-Control-Allow-Origin": "*" },
        "body": d if (type(d) == str or
                type(d) == unicode) else json.dumps(d)
        }
    if meta:
        res["meta"] = meta
    return res

def zip_with_binary(zipname, location, arcname):
    with ZipFile(zipname, "w") as f:
        f.write(location, arcname=arcname)
        #write each file to the zip file
        for root, dirs, files in os.walk("."):
            for file in files:
                f.write(os.path.join(root, file))